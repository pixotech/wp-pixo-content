<?php require __DIR__ . '/../inc/process_network_acf_sync_submit.inc.php'; ?>

<div>
    <h1>Sync all ACF fields across the Network</h1>
    <form method="post">
        <input class="button button-primary" id="sync" type="submit" name="sync" value="Sync" />
    </form>
</div>