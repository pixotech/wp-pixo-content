# Changelog for Pixo Content WordPress plugin

## 0.9.3
Remove `error_log()` call for running `pixo_parse_request_trick()` all the way through after seeing where it tends to do so.

## 0.9.2
- Bugfix: if duplicate slug prevention happens before checking protected slugs, we get duplicates.
- Clear up some PHP notices.
- Enable pixo_array_to_query_in_string() to not quote integers.

## 0.9.1
Rework of preventing duplicate slugs across post types.

## 0.9.0
- POSSIBLE BREAKING CHANGE: Page parent dropdown code left out pages and possibly others--get_posts() does not accept array of post types.

## 0.8.0
- Overhaul, stage 1, of code for custom post type paths with no post type in path and having parents of different post type.
- BREAKING CHANGES maybe: due to the above point.  If you wish for the post type name to be removed from the path on the post edit screen, add the following to your post type class:
	```php
	$this->__set('rewrite', [
        'slug' => '%remove_me%'
    ]);
	```

## 0.7.3
- Replaced deprecated wp_get_sites with get_sites.

## 0.7.2
- Lost args in CustomTaxonomy for WP REST API with a conflict merge.
- Force param for CustomTaxonomy::setDefaultTerms() to be an array.
- Add two methods to PixoPress: variableOperatorCompare() and arrayToQueryInString() as utilities.

## 0.7.1
- Custom taxonomies and post types now appear in REST API by default.  Added args to CustomTaxonomy and CustomPostType for WP REST API: show_in_rest, and rest_controller_base.
- Clean up taxonomy label defaults.

## 0.7.0
BREAKING CHANGES
- Add filter hook for reserving/protected custom permalink slugs: `pixo_protected_slugs_filter`.  Instructions for use are in README.
- Add filter hook for permastruct setting: `pixo_permalink_structure_filter`.

## 0.6.0
- Add filter hook for custom menu creation instead of creating a global array: pixo_custom_menus_hook.

## 0.5.4
- Add helper method to CustomPostType class for hierarchical post types: `enableHierarchical()`.

## 0.5.3
- Security fix: add_submenu_page for ACF Sync should check for manage_sites capability.
- Use wp button classes for acf sync form, and add id attr.
- Refactor pixo_get_list_of_custom_post_types() to accept filters as an arg. Refactor pixo_filter_page_parent_dropdown() to not drop the  passed to it and get_posts instead of direct db query.

## 0.5.2
- Update CHANGELOG for v0.5.0 and v0.5.1.

## 0.5.1
- Bugfix: Some custom field groups were getting duplicated by new ACF syncing feature.

## 0.5.0
- New feature for syncing ACF custom fields across a multisite network with one click.

## 0.4.4
- Ignore WP_Mock library.
- Add getSitePostTypes method to PixoPressNetwork, and remove old types_dir property.  Add classes for displaying messages to user: PixoPressMessage and PixoPressWPMessage.
- Create action hook for other plugins to toggle global taxonomy.

## 0.4.3
- Remove wp translate functions from CustomPostType to make testing easier.  Can be added back in subclasses.
- Namespace autoloader and move it into classes dir.

## 0.4.2
- Bugfix for enableWorkflow() method.

## 0.4.1
- Add enableWorkflow() method to CustomPostType, which sets `'delete_with_user' = false` and adds 'author' to 'supports'.
- Change capability_type to value of `$type_name` property for custom post types.
- New arg default for custom post types: map_meta_cap = true.
- Add start to a test for the CustomPostType class.

## 0.4.0

- IMPORTANT: You need to update how you extend this plugin's classes now that they are namespaced.  You won't need to require/include any class files anymore though.  
- Namespaced all the classes
- Added an autoloader class

## 0.3.1

- Removed setLabels() method from CustomPostType class for consistency, and it's no longer needed.
- Make the CustomPostType and CustomTaxonomy classes more consistent with each other.  
- Big README update for all the changes in 0.3.0. 

## 0.3.0

- Menu names now stored as global array.
- Custom post types now created via a custom hook, pixo_custom_post_types_hook, but you still extend the base CustomPostType class.
- Custom taxonomies now created via a custom hook, pixo_custom_taxonomies_hook, but you still extend the base CustomTaxonomy class.  This hook is invoked twice, once for registering taxonomies and once for creating default terms for each taxonomy.  Only operates on taxonomies that are returned with this new hook.
- Added class interfaces for both CustomPostType and CustomTaxonomy.
- Added a Label class for creating label variations, such as plural and machine name.
- Added Pluralize class for pluralizing labels.
- Added PixoPress class for things like static method to register custom post types.  
- Added support for reserving menu slugs
- Added several getter and setter methods in the post type and taxonomy base classes.

## 0.2.0

- Moved author UI related code to a new plugin: Pixo Author UI
