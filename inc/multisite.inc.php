<?php

use WordPress\Pixo\Content\PixoPressACFSync;

if(is_multisite()) {

    PixoPressACFSync::add_sync_network_subpage();

}
