<?php

function pixo_setup_custom_taxonomies() {
  add_action( 'init', 'pixo_register_custom_taxonomies', 1 );
  add_action( 'admin_init', 'pixo_create_custom_terms');
}

function pixo_register_custom_taxonomies() {
	apply_to_taxonomies(['\WordPress\Pixo\Content\PixoPress', 'register_taxonomy']);
}

function pixo_create_custom_terms() {
	apply_to_taxonomies(['\WordPress\Pixo\Content\PixoPress', 'create_taxonomy_terms']);
}

function apply_to_taxonomies($callable) {
	$taxonomies = apply_filters('pixo_custom_taxonomies_hook', []);
	array_map($callable, $taxonomies);
}
