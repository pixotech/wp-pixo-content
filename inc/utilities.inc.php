<?php 

function pixo_add_upload_mime_types() {
  add_filter( 'upload_mimes', 'pixo_expand_upload_mime_types' );
}

function pixo_set_permalink_structure() {
  $default_format = '/%postname%/';
  $format = apply_filters('pixo_permalink_structure_filter', $default_format);
  update_option( 'permalink_structure', $format );
}

function pixo_expand_upload_mime_types($existing_mimes = array()) {
  $existing_mimes['svg'] = 'image/svg+xml';
  return $existing_mimes;
}

/**
 * Format an array into a string for use in MySQL
 * WHERE ... IN () clause.
 *
 * @param (array) $vals
 * @return (string) values in single quotes separated by comma
 */
function pixo_array_to_query_in_string($vals) {
  $return = '';
  $i = 1;
  $count = count($vals);
  foreach ($vals as $v) {
      if(is_int($v)) {
          $return .= "$v";
      } else {
          $return .= "'$v'";
      }
    if($i < $count)
      $return .= ',';
    $i++;
  }
  return $return;
}

if(!function_exists('dbg')) {
  function dbg($data, $title = false) {
    if($title)
      print '<h1>'.$title.'</h1>';
    print '<pre>';
    print htmlentities(print_r($data, true));
    print '</pre>';
  }
}

function pixo_write_to_file($data, $filename, $append = false) {
  if($append) {
    file_put_contents($filename, $data, FILE_APPEND);
  } else {
    $f = fopen($filename, 'w');
    fwrite($f, $data);
    fclose($f);
  }
}