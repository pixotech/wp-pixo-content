<?php

use WordPress\Pixo\Content\PixoPress;

function pixo_setup_custom_post_types() {
  add_action( 'init', 'pixo_register_custom_post_types', 0 );
  pixo_custom_post_type_slugs();
}

function pixo_register_custom_post_types() {
  $post_types = [];
  $post_types = apply_filters('pixo_custom_post_types_hook', $post_types);
  if(is_array($post_types)) {
    foreach ($post_types as $Type)
      PixoPress::register_post_type($Type);
  }
}

function pixo_custom_post_type_slugs() {
  if(NO_CUSTOM_POST_TYPE_SLUG) {
     add_filter( 'post_link', 'pixo_remove_cpt_slug', 10, 3 ); // not called for post edit screen (at least for custom post types and the page type)
    add_filter( 'post_type_link', 'pixo_remove_cpt_slug_post_edit_screen', 10, 3); // called for post edit screen (at least for custom post types, but not seemingly called for page post type)
      /// TODO: not sure this is actually doing anything anymore.  Placed error_log() at its end to see if it does ever run.
      add_action( 'pre_get_posts', 'pixo_parse_request_trick' );
  }
}

/**
* Remove the slug from published post permalinks. Only affect our CPT though.
* This borrowed from http://kellenmace.com/remove-custom-post-type-slug-from-permalinks/
*/
function pixo_remove_cpt_slug( $post_link, $post, $leavename ) {
    if ( in_array($post->post_type, ['page', 'post', 'attachment']) || 'publish' != $post->post_status ) {
		return $post_link;
	}

    if( $post->post_parent ) {
        $parents = pixo_get_parents($post);


        if(strpos($post_link, $post->post_type) !== false) {
            $post_link = str_replace( "/{$post->post_type}/", $parents, $post_link );
        } else {
            $post_link = str_replace( "/{$post->post_name}/", "{$parents}{$post->post_name}/", $post_link);
        }

    } else {
        $post_link = str_replace("/{$post->post_type}/", '/', $post_link);
    }
    $post_link = str_replace('/%remove_me%/', '/', $post_link);

    return $post_link;
}

function pixo_get_parents($post)
{
    $ancestors = [];
    $parent = $post;
    do {
        $parent = get_post($parent->post_parent);
        $ancestors[] = $parent->post_name;
    } while ($parent->post_parent != 0);

    return '/'.implode('/', array_reverse($ancestors)).'/';
}

function pixo_remove_cpt_slug_post_edit_screen($post_link, $post, $leavename)
{
    $post_link = str_replace('/%remove_me%/', '/', $post_link);
    return $post_link;
}

/**
 * Have WordPress match postname to any of our public post types (page, post, race)
 * All of our public post types can have /post-name/ as the slug, so they better be unique across all posts
 * By default, core only accounts for posts and pages where the slug is /post-name/
 * This borrowed from http://kellenmace.com/remove-custom-post-type-slug-from-permalinks/
 */
function pixo_parse_request_trick( $query ) {
    // Only noop the main query
    if ( ! $query->is_main_query() ) {
        return;
    }

    // Only noop our very specific rewrite rule match
    if ( 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
        return;
    }

    // 'name' will be set if post permalinks are just post_name, otherwise the page rule will match
  // NOTE: if 'post' is not the first element on the following array, then
  // they'll return a 404 when trying to visit a post in browser.
  $post_types = array_keys(pixo_get_list_of_custom_post_types());
  array_unshift($post_types, 'post');
  array_push($post_types, 'page');
  if ( ! empty( $query->query['name'] ) )
    $query->set( 'post_type', $post_types );
}

function pixo_enable_cross_type_slug_uniqueness()
{
    add_filter('wp_unique_post_slug_is_bad_hierarchical_slug', 'pixo_ensure_unique_slugs_across_hierarchical_post_types', 35, 4);
}

/**
 * @param $protected
 * @param $slug
 * @param $post_type
 * @param $post_parent
 * @return bool
 *
 * Enables post parents to be of different post type and
 * ensures slugs remain unique.  Does NOT solve problem
 * of 404's for such posts in coupled CMS scenarios.
 */
function pixo_ensure_unique_slugs_across_hierarchical_post_types($protected, $slug, $post_type, $post_parent)
{
    if(!isset($_REQUEST) || !isset($_REQUEST['action']) || !in_array($_REQUEST['action'], ['sample-permalink', 'editpost', 'edit']))
        return $protected;

    if(!isset($_REQUEST['post_id']) && !(isset($_REQUEST['post'])))
        return $protected;
    $post_id = (isset($_REQUEST['post_id'])) ? $_REQUEST['post_id'] : $_REQUEST['post'];

    $hierarchical_post_types = PixoPress::arrayToQueryInString( get_post_types(['hierarchical' => true]) );

    global $wpdb;
    $names = $wpdb->get_col( $wpdb->prepare("SELECT post_name FROM $wpdb->posts WHERE post_name != '' AND post_type IN ($hierarchical_post_types) AND post_parent = %d AND ID != %d", $post_parent, $post_id));
    if(in_array($slug, $names))
        $protected = (bool) 1;
    return $protected;
}
/**
 * @param array $filter gets passed directly to get_post_types(),
 *              defaults to ['public' => true].
 * @return array of post types.
 */
function pixo_get_list_of_custom_post_types($filters = ['public' => true]) {
	$post_types = get_post_types($filters);
	unset($post_types['post'], $post_types['page'], $post_types['attachment']);
    return $post_types;
}

function pixo_filter_page_parent_dropdown($pages, $r) {
  $post_types = get_post_types(['public' => true, 'hierarchical' => true]);
    unset($post_types[$r['post_type']]);
    global $wpdb;
    $post_types_for_IN_clause = PixoPress::arrayToQueryInString($post_types);
    $other_posts = $wpdb->get_results( $wpdb->prepare(
        "SELECT * FROM {$wpdb->posts} 
        WHERE post_type IN ($post_types_for_IN_clause) AND
          post_status = 'publish'
        ORDER BY %s, %s ASC", ['menu_order', 'post_title']
    ));

    return array_unique( array_merge($pages, $other_posts), SORT_REGULAR );
}
add_filter('get_pages', 'pixo_filter_page_parent_dropdown', 10, 2);
