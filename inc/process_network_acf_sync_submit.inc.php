<?php

use WordPress\Pixo\Content\PixoPressACFSync;

if(isset($_POST) && isset($_POST['sync'])) {
    PixoPressACFSync::sync();
}
