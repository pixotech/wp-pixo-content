<?php // menus.inc.php

function pixo_create_custom_menus() {
	add_action( 'init', 'pixo_create_menus');
}

function pixo_create_menus() {
	$menus = apply_filters('pixo_custom_menus_hook', []);
	if(is_array($menus) && !empty($menus)) {
		foreach ($menus as $menu) {
			if(!wp_get_nav_menu_object($menu)) {
				$menu_id = wp_create_nav_menu($menu);
				if(is_wp_error($menu_id))
					echo $menu_id->get_error_message();
			}
		}
	}
}

function pixo_setup_reorder_admin_menus() {
	add_filter( 'custom_menu_order', '__return_true' );
	add_filter( 'menu_order', 'pixo_reorder_admin_menus' );
}

// See below this function's return for an example from another project on how to reorder the admin menu items.
function pixo_reorder_admin_menus($menu_order) {

	return apply_filters('pixo_custom_admin_menus_order_hook', $menu_order);

	/*** Example: ***/
	// $landing_page_path = 'edit.php?post_type=landing_page';
	// $pages_path = 'edit.php?post_type=page';

	// // Move Pages beneath Landing Page
	// $landing_page = array_search($landing_page_path, $menu_order);
	// $pages = array_search($pages_path, $menu_order);
	// unset($menu_order[$pages]);

	// // Put the pieces back together
	// $first = array_slice($menu_order, 0, $landing_page + 1);
	// $second = array_slice($menu_order, $landing_page + 1);
	// $first[] = $pages_path;
	// $new_menu = array_merge($first, $second);

	// return $new_menu;
}

function pixo_reserve_permalink_slugs() {
	add_filter('wp_unique_post_slug_is_bad_hierarchical_slug', 'pixo_protect_reserved_slugs', 10, 4);
	add_filter('wp_unique_post_slug_is_bad_flat_slug', 'pixo_protect_reserved_slugs', 10, 3);
}

function pixo_protect_reserved_slugs($is_bad_slug, $slug, $post_type, $post_parent = null) {
	if($slugs = apply_filters('pixo_protected_slugs_filter', [])) {
		if(in_array($slug, $slugs['all']))
			return true;
		if(isset($slugs[$post_type]) && in_array($slug, $slugs[$post_type]))
  			return true;
	}
	return false;
}
