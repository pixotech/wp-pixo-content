# Pixo Content Plugin README

This plugin adds custom post types, custom taxonomies, creates menus, provides support for reserving menu slugs, and supplies some utility functions.  

The custom post types and taxonomies are created with hooks and extending the base classes provided by this plugin.

## Permalinks
This plugin defaults to setting permalink structure to `'/%postname%/'` because it's needed for custom post types.  However, a filter is provided for overriding this default: `pixo_permalink_structure_filter`.  

### Protect/Reserve Permalink Slugs
Implement the `pixo_protected_slugs_filter` filter, returning a multi-dimensional array where the keys for the outer array are either post type machine names or 'all' to apply the reservation to all post types.  For example:
```php
add_filter('pixo_protected_slugs_filter', 'my_protected_slugs');
function my_protected_slugs($slugs) {
    return array_unique( array_merge( $slugs, [
        'all' => [
            'news'
        ],
        'landing-page' => [
            'very-special-slug'
        ]
    ] ));
}
```

## Custom Post Types

### Custom Post Type Slugs
The NO_CUSTOM_POST_TYPE_SLUG constant is set to TRUE to prevent posts of custom post types always having the post type name as the start of the slug.  For example, if the constant is set to FALSE, then the slug for all posts of a custom type called Landing Page would start with "/landing-page/".  You can override this on a per-post-type basis by setting the `['rewrite' => ['slug' => 'different-permastruct']]` argument in your custom post type class.

### Create a Custom Post Type
1. Extend [classes/CustomPostType.class.php](classes/CustomPostType.class.php).  See [examples/Example.CPT.class.php](examples/Example.CPT.class.php) for example.
1. Implement the `pixo_custom_post_types_hook` filter hook, returning an array of custom post type objects, like this example:

```php
add_filter('pixo_custom_post_types_hook', 'myplugin_custom_post_types');
function myplugin_custom_post_types($post_types) {
  require_once __DIR__ . '/custom-post-types/PersonPostType.CPT.class.php';
  $post_types[] = new PersonPostType();
  return $post_types;
}
```

## Custom Taxonomies
Custom taxonomies are handled very similarly to custom post types.

### Create a custom taxonomy:
1. Extend [classes/CustomTaxonomy.class.php](classes/CustomTaxonomy.class.php).  See [examples/Examples.CTax.class.php](examples/Examples.CTax.class.php) for example.
1. Implement the `pixo_custom_taxonomies_hook' filter hook, returning an array of custom taxonomy objects, like this example:

```php
add_filter('pixo_custom_taxonomies_hook', 'myplugin_custom_taxonomies');
function myplugin_custom_taxonomies($taxonomies) {
  require_once __DIR__ . '/custom-taxonomies/PersonTaxonomy.CTax.class.php';
  $taxonomies[] = new PersonTaxonomy();
  return $taxonomies;
}
```

### Set default terms for a taxonomy:
1. Populate the default_terms property of the CustomTaxonomy class using the built-in setDefaultTerms() method, which expects an indexed array of human-readable term names.  There's nothing else to do.  The same hook used to create the taxonomy will also be called when inserting the default terms into the database.

### Set taxonomy to be global across multisite network
```php
add_action('pixo_global_taxonomy', function() { return true; });
```

## Menus
To programmatically create empty menus, and ensure they exist, implement the pixo_custom_menus_hook filter and return an indexed array of human-readable menu names.  

### Reordering admin menus
Additionally, there's an example function from a previous project
that shows how to reorder admin menu items if desired.  You will
need to make sure that `pixo_setup_reorder_admin_menus()` is uncommented in [pixo-content.php](pixo-content.php).

## Utilities
The utilities include contains a few useful functions, as well as
the supporting logic for setting permalink structure and adding
additional allowed upload mime types.
