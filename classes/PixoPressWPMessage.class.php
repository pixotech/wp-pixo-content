<?php

namespace WordPress\Pixo\Content;

class PixoPressWPMessage extends PixoPressMessage {

  public $message;
  public $level;
  public $type;

  /**
   * @param string $message The message
   * @param string $level options are:
   *        'error' produces red stripe
   *        'notice' produces no stripe
   *        'notice-success' produces green stripe
   *        'notice-info' produces blue stripe
   *        'update-nag' produces orange stripe.
   * @param string $type options are:
   *        'admin_notices' || 'network_admin_notices' || 'all_admin_notices'
   */
  public function __construct($message, $level = 'error', $type = 'admin_notices')
  {
    $this->message = $message;
    $this->level = $level;
    $this->type = $type;
    $this->do_wp_notice($this->type, $this->wp_notice_message($this->level, $this->message));
  }

  function do_wp_notice()
  {
    add_action($this->type, $this->message);  
  }

  function wp_notice_message()
  {
    print '<div class="' . $this->level . ' notice"><p>' . $this->message . '</p></div>';
  }
}