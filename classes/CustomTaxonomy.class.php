<?php

namespace WordPress\Pixo\Content;

class CustomTaxonomy implements CustomTaxonomyInterface {
    protected $args = []; // Overloaded
    protected $labels;
    protected $name;
    protected $singular_name;
    protected $plural_name;
    protected $object_types = [];
    protected $default_terms = [];

    public function __construct($singular_name, $object_types = FALSE) {
        $this->singular_name = $singular_name;
        $this->plural_name = Label::make_plural($this->singular_name);
        $this->name = Label::make_machine_name($this->plural_name);
        if($object_types)
            $this->object_types = $object_types;
        $this->set_default_labels();
        $this->set_default_args();
    }

    public function __get($arg) {
        if( array_key_exists($arg, $this->args))
            return $this->args[$arg];

        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $arg .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
        return null;
    }

    public function __set($arg, $val) {
        $this->args[$arg] = $val;
    }

    protected function set_default_args() {
        $this->args = [
            'labels'                            => [],
            'hierarchical'        => true,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_rest'        => true,
            'rest_controller_base' => 'WP_REST_Terms_Controller'
        ];
    }

    protected function set_default_labels() {
        $this->labels = [
            'name'                => _x( $this->plural_name, 'Post Type General Name', 'pixo_custom' ),
            'singular_name'       => _x( $this->singular_name, 'Post Type Singular Name', 'pixo_custom' ),
            'menu_name'           => __( "$this->plural_name", 'pixo_custom' ),
            'parent_item_colon'   => __( "Parent $this->singular_name:", 'pixo_custom' ),
            'all_items'           => __( "All $this->plural_name", 'pixo_custom' ),
            'edit_item'           => __( "Edit $this->singular_name", 'pixo_custom' ),
            'view_item'           => __( "View $this->singular_name", 'pixo_custom' ),
            'update_item'         => __( "Update $this->singular_name", 'pixo_custom' ),
            'add_new_item'        => __( "Add New", 'pixo_custom' ),
            'search_items'        => __( "Search $this->plural_name", 'pixo_custom' ),
        ];
    }

    public function getFullArgs() {
        $argsCopy = $this->args;
        $argsCopy['labels'] = $this->labels;
        return $argsCopy;
    }

    protected function hide_taxonomy() {
        $this->__set('public', false);
        $this->__set('show_ui', false);
        $this->__set('show_in_menu', false);
        $this->__set('show_in_nav_menus', false);
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getDefaultTerms()
    {
        return $this->default_terms;
    }

    /**
     * @param (array) $default_terms should be an
     * indexed array of human-readable term names.
     */
    public function setDefaultTerms($default_terms)
    {
        $this->default_terms = (array) $default_terms;
    }

    public function getObjectTypes()
    {
        return $this->object_types;
    }

    public function setObjectTypes($object_types)
    {
        $this->object_types = $object_types;
    }
}
