<?php 

namespace WordPress\Pixo\Content;

use Pluralize;

require_once __DIR__ . '/../vendor/pluralize-php-master/pluralize.php';

class Label {
  public static function make_plural($singular_label) {
    return ucwords(Pluralize::pluralize($singular_label, 3));
  }

  public static function make_machine_name($human_name) {
    return str_replace(' ', '_', strtolower($human_name));
  }
}