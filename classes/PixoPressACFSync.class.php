<?php

namespace WordPress\Pixo\Content;

class PixoPressACFSync {

    protected static $sync;
    protected static $sync_admin_page_template = 'network_acf_sync_admin.php';

    public static function add_sync_network_subpage()
    {
        add_action('network_admin_menu', [get_called_class(), 'sync_network_subpage']);
    }

    public static function sync_network_subpage()
    {
        add_submenu_page('sites.php', 'ACF Custom Fields Network Sync', 'ACF Network Sync', 'manage_sites', 'acf-network-sync', [get_called_class(), 'get_sync_admin_page_template']);
    }

    public static function get_sync_admin_page_template()
    {
        include PixoPress::getPixoPressTemplatesDir() . '/' . self::$sync_admin_page_template;
    }

    public static function sync()
    {
        $sites = PixoPress::get_list_of_microsites();
        foreach ($sites as $site) {
            switch_to_blog($site['blog_id']);
            self::syncACFFields();
        }
        restore_current_blog();
        new PixoPressWPMessage(sprintf('Fields sync\'d across %d sites in the network', count($sites)), 'notice-success', 'network_admin_notices');
    }

    protected static function syncACFFields()
    {
        /// TODO: Tweak things like admin_check_referrer to work with our approach.

        // vars
        $groups = acf_get_field_groups();
        
        
        // bail early if no field groups
        if( empty($groups) ) {
            
            return;
            
        }
        
        
        // find JSON field groups which have not yet been imported
        foreach( $groups as $group ) {
            
            // vars
            $local = acf_maybe_get($group, 'local', false);
            $modified = acf_maybe_get($group, 'modified', 0);
            $private = acf_maybe_get($group, 'private', false);
            
            
            // ignore DB / PHP / private field groups
            if( $local !== 'json' || $private ) {
                
                // do nothing
                
            } elseif( !$group['ID'] ) {
                
                self::$sync[ $group['key'] ] = $group;
                
            } elseif( $modified && $modified > get_post_modified_time('U', true, $group['ID'], true) ) {
                
                self::$sync[ $group['key'] ]  = $group;
                
            }
                        
        }
        
        
        // bail if no sync needed
        if( empty(self::$sync) ) {
            
            return;
            
        }

        // if( !empty($keys) ) {
            
            // disable JSON
            // - this prevents a new JSON file being created and causing a 'change' to theme files - solves git anoyance
            acf_update_setting('json', false);
            
            // vars
            $new_ids = array();
            
            foreach( self::$sync as $key => $v ) { // foreach( $keys as $key ) {
                
                // append fields
                if( acf_have_local_fields( $key ) ) {
                    
                    self::$sync[ $key ]['fields'] = acf_get_local_fields( $key );
                    
                }
                
                
                // import
                $field_group = acf_import_field_group( self::$sync[ $key ] );
                
            }
            
        // }
    }
}