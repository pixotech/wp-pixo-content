<?php

namespace WordPress\Pixo\Content;

class Autoloader {

  protected $prefix;
  protected $dir;

  public function __construct($prefix, $dir) {
    $this->prefix = $prefix;
    $this->dir = $dir;
  }

  public function __invoke($c) {
    if (substr($c, 0, strlen($this->prefix)) == $this->prefix) {
      $segments = explode('\\', substr($c, strlen($this->prefix)));
      $path = $this->makePath($segments);
      if (file_exists($path)) include_once($path);
    }
    return class_exists($c);
  }

  protected function makePath($segments) {
    return  $this->dir . implode('/', $segments) . '.class.php';
  }
}
