<?php

namespace WordPress\Pixo\Content;

class CustomPostType implements CustomPostTypeInterface {
  protected $args = []; // Overloaded
  protected $labels;
  protected $type_name;
  protected $type_singular;
  protected $type_plural;
  protected $description;

  public function __construct($singular_name, $description) {
    $this->type_singular = $singular_name;
    $this->setType_plural();
    $this->setType_name();
    $this->setDescription($description);
    $this->set_default_labels();
    $this->set_default_args();
  }

  public function __get($arg) {
    if( array_key_exists($arg, $this->args))
      return $this->args[$arg];

    $trace = debug_backtrace();
    trigger_error(
        'Undefined property via __get(): ' . $arg .
        ' in ' . $trace[0]['file'] .
        ' on line ' . $trace[0]['line'],
        E_USER_NOTICE);
    return null;
  }

  public function __set($arg, $val) {
    $this->args[$arg] = $val;
  }

  public function getType_name() {
    return $this->type_name;
  }

  public function setType_name($name = false) {
    if($name) {
      $this->type_name = $name;
    } else {
      $this->type_name = Label::make_machine_name($this->type_singular);
    }
  }

  public function getArgs() {
    return $this->args;
  }

  public function getFullArgs() {
    $argsCopy = $this->args;
    $argsCopy['labels'] = $this->labels;
    return $argsCopy;
  }

  public function getType_singular() {
    return $this->type_singular;
  }

  public function getType_plural() {
    return $this->type_plural;
  }

  public function setType_plural($plural_name = false) {
    if($plural_name) {
      $this->type_plural = $plural_name;
    } else {
      $this->type_plural = Label::make_plural($this->type_singular);
    }
  }

  public function getDescription() {
    return $this->description;
  }

  public function setDescription($description) {
    $this->description = $description;
  }

  protected function enableWorkflow($author = true) {
    if(!in_array('author', $this->args['supports']))
      $this->args['supports'][] = 'author';
    $this->__set('delete_with_user', false);
  }

  protected function enableHierarchical()
  {
    array_push($this->args['supports'], 'page-attributes');
    $this->args['hierarchical'] = true;
  }

  /**
   * The supports argument takes the following values: title, editor, comments, revisions, trackbacks, author, excerpt, page-attributes, thumbnail, custom-fields, post-formats.
   * For a list of all the args and options, see 
   * https://developer.wordpress.org/reference/functions/register_post_type/.  
   */
  protected function set_default_args() {
    $this->args = [
      'labels'              => [],
      'label'               => $this->getType_singular(),
      'description'         => $this->getDescription(),
      'supports'            => array( 'title', 'revisions' ),
      'hierarchical'        => true,
      'public'              => true,
      'show_ui'             => true,
      'show_in_menu'        => true,
      'show_in_nav_menus'   => true,
      'show_in_admin_bar'   => true,
      'menu_position'       => 5,
      'can_export'          => true,
      'has_archive'         => true,
      'exclude_from_search' => false,
      'publicly_queryable'  => true,
      'capability_type'     => $this->type_name,
      'map_meta_cap'        => true,
      'show_in_rest'        => true,
      'rest_controller_base' => 'WP_REST_Posts_Controller'
    ];
  }

  protected function set_default_labels() {
    $this->labels = [
      'name'                => $this->getType_plural(),
      'singular_name'       => $this->getType_singular(),
      'menu_name'           => $this->getType_singular(),
      'parent_item_colon'   => "Parent $this->type_singular:",
      'all_items'           => "All $this->type_plural",
      'view_item'           => "View $this->type_singular",
      'add_new_item'        => "Add New " . $this->getType_singular(),
      'add_new'             => "Add New",
      'edit_item'           => "Edit $this->type_singular",
      'update_item'         => "Update $this->type_singular",
      'search_items'        => "Search $this->type_singular",
      'not_found'           => 'Not found',
      'not_found_in_trash'  => 'Not found in Trash',
    ];
  }

}
