<?php

/**
 * Created by PhpStorm.
 * User: marty
 * Date: 3/21/16
 * Time: 3:24 PM
 */

namespace WordPress\Pixo\Content;

interface CustomTaxonomyInterface
{
    public function __get($arg);

    public function __set($arg, $val);
}