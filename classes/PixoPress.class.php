<?php

namespace WordPress\Pixo\Content;

class PixoPress {

  public static function register_post_type(CustomPostType $CustomPostType) {
    register_post_type( $CustomPostType->getType_name(), $CustomPostType->getFullArgs() );
  }

  public static function register_taxonomy(CustomTaxonomy $CustomTaxonomy) {
    $custom_taxonomy_object_types = $CustomTaxonomy->getObjectTypes();
    register_taxonomy( $CustomTaxonomy->getName(), $custom_taxonomy_object_types, $CustomTaxonomy->getFullArgs() );

    if(is_array($custom_taxonomy_object_types) && !empty($custom_taxonomy_object_types)) {
      
      foreach ($custom_taxonomy_object_types as $type) {
          register_taxonomy_for_object_type($CustomTaxonomy->getName(), $type);
      }
      
    } elseif (is_string($custom_taxonomy_object_types) && $custom_taxonomy_object_types != '') {
      $type = $custom_taxonomy_object_types;
    }

    if (!empty($type)) {
      register_taxonomy_for_object_type($CustomTaxonomy->getName(), $type);
    }
  }

  public static function create_taxonomy_terms(CustomTaxonomy $CustomTaxonomy) {
    $terms = $CustomTaxonomy->getDefaultTerms();

    foreach($terms as $term){
      wp_create_term($term, $CustomTaxonomy->getName());
    }
  }

  public static function get_list_of_microsites()
  {
    if(!is_multisite())
      return false;
    $sites = get_sites();
    if(is_multisite() && empty($sites))
      throw new \Exception("get_sites returned empty array, indicates network is too large (>= 10,000 sites).");
    return $sites;
  }

  public static function getPixoPressTemplatesDir()
  {
    return __DIR__ . '/../templates';
  }

  public static function variableOperatorCompare($left_value, $operator, $right_value)
  {
    switch ($operator) {
        case '!=':
            return $left_value != $right_value;
            break;
        
        case '==':
            return $left_value == $right_value;
            break;
    }
    return false;
  }

  /**
   * Format an array into a string for use in MySQL
   * WHERE ... IN () clause.
   *
   * @param (array) $vals
   * @return (string) values in single quotes separated by comma
   */
  public static function arrayToQueryInString($vals)
  {
    $return = '';
    $i = 1;
    $count = count($vals);
    foreach ($vals as $k => $v) {
      $return .= "'$k'";
      if($i < $count)
        $return .= ',';
      $i++;
    }
    return $return;
  }
}
