<?php 

namespace WordPress\Pixo\Content;

interface CustomPostTypeInterface {

  public function __get($arg);

  public function __set($arg, $val);

}