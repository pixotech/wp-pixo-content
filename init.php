<?php

require_once __DIR__ . '/classes/Autoloader.class.php';

spl_autoload_register(new \WordPress\Pixo\Content\Autoloader('WordPress\\Pixo\\Content', __DIR__ . '/classes/'));
spl_autoload_register(new \WordPress\Pixo\Content\Autoloader('', __DIR__ . '/vendor/'));