<?php 

require_once 'CustomPostType.class.php';

class Example extends CustomPostType {

	public function __construct() {
		$this->type_singular = 'Example';
		$this->description = 'Pixo Example Custom Post Type';

		parent::__construct($this->type_singular, $this->description);
		$this->override_default_args();
	}

	private function override_default_args() {
		$this->__set('label', __( $this->getType_name(), 'pixo_custom' ));
		$this->__set('description', __( $this->getDescription(), 'pixo_custom' ));
		$this->__set('hierarchical', false);
		$this->__set('public', false);
		$this->__set('exclude_from_search', true);
		$this->__set('capability_type', 'post');
		$this->__set('menu_position', 10);
		$this->labels['all_items'] = __( $this->getType_plural(), 'pixo_custom' );
		$this->labels['name'] = _x( 'Pixo ' . $this->getType_plural(), 'Post Type General Name', 'pixo_custom' );
	}
}