<?php 

require_once 'CustomTaxonomy.class.php';

class ExamplesTax extends CustomTaxonomy {

	public function __construct() {
		$this->singular_name = 'Example Type';
		$this->object_types = ['example'];

		parent::__construct($this->singular_name, $this->object_types);
		$this->override_default_args();
		$this->hide_taxonomy();
	}

	private function override_default_args() {
		$this->__set('hierarchical', false);
	}
}