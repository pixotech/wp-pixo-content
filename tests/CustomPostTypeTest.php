<?php

class CustomPostTypeTest extends PHPUnit_Framework_TestCase {
  public function testEnableWorkflow() {
    $PostType = new CustomPostType('Custom Post Type', 'Description for custom post type.');
    $PostType->enableWorkflow();
    $args = $PostType->getArgs();
    $this->assertTrue($args['map_meta_cap']);
    $this->assertTrue(in_array('author', $args['supports']));
  }
}