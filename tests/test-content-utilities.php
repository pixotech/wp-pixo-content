<?php

/**
 * Ensure that utility functions are functioning.
 *
 * @group pixo_content
 *
 * @package WordPress
 * @subpackage Pixo Content
 */
class WP_Test_Pixo_Content_Utilities extends WP_UnitTestCase {

	public function setUp() {
		parent::setUp();
	}

	public function test_pixo_permalink_option() {
		$this->assertEquals('/%postname%/', get_option('permalink_structure'));
	}

	public function teardown() {
		parent::teardown();
	}
}