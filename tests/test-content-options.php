<?php

/**
 * Ensure that options are functioning.
 *
 * @group pixo_content
 *
 * @package WordPress
 * @subpackage Pixo Content
 */
class WP_Test_Pixo_Content_Options extends WP_UnitTestCase {

	public function setUp() {
		parent::setUp();
	}

	

	public function teardown() {
		parent::teardown();
	}
}