<?php

/**
 * Ensure that expected menus are created and retrievable.
 *
 * @group pixo_content
 *
 * @package WordPress
 * @subpackage Pixo Content
 */
class WP_Test_Pixo_Content_Menus extends WP_UnitTestCase {

	public function setUp() {
		parent::setUp();
	}

	public function teardown() {
		parent::teardown();
	}

	public function test_pixo_eyebrow_menu() {
		$this->assertMenuExists('Eyebrow');
	}

	public function test_pixo_main_nav_menu() {
		$this->assertMenuExists('Main Nav');
	}

	public function test_pixo_local_nav_menu() {
		$this->assertMenuExists('Local Nav');
	}

	public function test_pixo_footer_quick_links_menu() {
		$this->assertMenuExists('Footer Quick Links');
	}

	public function test_pixo_footer_resources_menu() {
		$this->assertMenuExists('Footer Resources');
	}

	public function test_pixo_footer_tools_menu() {
		$this->assertMenuExists('Footer Tools');
	}

	public function test_pixo_footer_buttons_menu() {
		$this->assertMenuExists('Footer Buttons');
	}

	private function assertMenuExists($menu_name) {
		$menu = wp_get_nav_menu_object($menu_name);
		$this->assertInstanceOf('stdClass', $menu);
		$this->assertObjectHasAttribute('term_id', $menu);
		$this->assertObjectHasAttribute('name', $menu);
		$this->assertObjectHasAttribute('slug', $menu);
		$this->assertObjectHasAttribute('parent', $menu);
		$this->assertObjectHasAttribute('count', $menu);
		$this->assertEquals($menu_name, $menu->name);
	}
}