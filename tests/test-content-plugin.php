<?php

/**
 * Base plugin tests to ensure the Pixo Content plugin is loaded correctly. 
 *
 * @package WordPress
 * @subpackage Pixo Content
 */
class WP_Test_Pixo_Content_Plugin extends WP_UnitTestCase {

	public function setUp() {
		parent::setUp();
	}

	public function teardown() {
		parent::teardown();
	}

	/**
	 * The plugin should be installed and activated.
	 */
	// function test_plugin_activated() {
	// 	$this->assertTrue( class_exists( 'Pixo_JSON_Menus' ) );
	// }

	// public function test_pixo_modules() {
	// 	$opts = $GLOBALS['wp_tests_options'];
	// 	$expected = [
	// 		'active_plugins' => [
	// 			'acf-to-wp-api/acf-to-wp-api.php',
	// 			'advanced-custom-fields-pro/acf.php',
	// 			'json-rest-api/plugin.php',
	// 			'pixo-wp-api/pixo-wp-api.php',
	// 			'pixo-content/pixo-content.php',
	// 			'wordfence/wordfence.php'
	// 		],
	// 	];
	// 	$this->assertEquals($expected, $opts);
	// 	print '<pre>';
	// 	print_r($opts);
	// 	print '</pre>';
	// }

	public function test_pixo_custom_post_types_action_added() {
		$this->assertEquals( 0, has_action( 'init', 'pixo_register_custom_post_types' ) );
	}

	public function test_pixo_post_parse_request_action_added() {
		$this->assertEquals( 10, has_action('pre_get_posts', 'pixo_parse_request_trick'));
	}

	public function test_pixo_remove_cpt_slug_filter_added() {
		$this->assertEquals( 10, has_filter('post_type_link', 'pixo_remove_cpt_slug'));
	}

	public function test_pixo_acf_toolbars_filter_added() {
		$this->assertEquals( 10, has_filter('acf/fields/wysiwyg/toolbars', 'pixo_acf_toolbars'));
	}

	public function test_pixo_standard_tinyMCE_filter_added() {
		$this->assertEquals( 10, has_filter('tiny_mce_before_init', 'pixo_standard_tinyMCE'));
	}

	public function test_pixo_remove_media_buttons_filter_added() {
		$this->assertEquals( 10, has_filter('wp_editor_settings', 'pixo_remove_media_buttons'));
	}

	public function test_pixo_tinyMCE_external_plugins_filter_added() {
		$this->assertEquals( 10, has_filter('mce_external_plugins', 'pixo_tinyMCE_external_plugins'));
	}

	public function test_pixo_post_preview_link_filter_added() {
		$this->assertEquals( 10, has_filter('preview_post_link', 'pixo_custom_preview_link'));
	}

	public function test_pixo_page_preview_link_filter_added() {
		$this->assertEquals( 10, has_filter('preview_page_link', 'pixo_custom_preview_link'));
	}
}
