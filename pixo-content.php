<?php
/*
Plugin Name: Pixo Content
Plugin URI: http://www.pixotech.com/
Description: Content related customizations, such as for custom post types, taxonomies, menus.
Author: Copyright (C) 2016 On the Job Consulting Inc. d/b/a Pixo
Version: 0.9.3
Author URI: http://www.pixotech.com/
License: BSD-3
*/

require_once 'inc/utilities.inc.php';
require_once 'init.php';
require_once 'inc/custom-post-types.inc.php';
require_once 'inc/custom-taxonomies.inc.php';
require_once 'inc/menus.inc.php';
require_once 'inc/multisite.inc.php';

// Custom Post Types and Taxonomies
define('NO_CUSTOM_POST_TYPE_SLUG', TRUE);
pixo_setup_custom_post_types();
pixo_setup_custom_taxonomies();
pixo_enable_cross_type_slug_uniqueness();

// Menus
pixo_create_custom_menus();
pixo_setup_reorder_admin_menus();

// Configure WP options
pixo_set_permalink_structure();
pixo_add_upload_mime_types();

// Reserve permalink slugs
pixo_reserve_permalink_slugs();

